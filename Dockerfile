FROM php:7.3-fpm

RUN mkdir -p /var/www/html

COPY src/app.php /var/www/html/app.php

EXPOSE 9000
